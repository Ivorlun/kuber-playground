FROM google/cloud-sdk:alpine

ARG HELM_VER="v3.11.0"
ARG HELMFILE_VER="0.150.0"

RUN curl -sLO https://get.helm.sh/helm-${HELM_VER}-linux-amd64.tar.gz && \
    curl -sL https://get.helm.sh/helm-${HELM_VER}-linux-amd64.tar.gz.sha256sum | sha256sum -c && \
    tar -zxf helm-${HELM_VER}-linux-amd64.tar.gz linux-amd64/helm && \
    mv -v linux-amd64/helm /usr/local/bin && \
    rm -f helm-${HELM_VER}-linux-amd64.tar.gz && rmdir linux-amd64

RUN curl -sLO https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VER}/helmfile_${HELMFILE_VER}_linux_amd64.tar.gz && \
    curl -sL https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VER}/helmfile_${HELMFILE_VER}_checksums.txt | grep linux_amd64 | sha256sum -c && \
    tar -zxf helmfile_${HELMFILE_VER}_linux_amd64.tar.gz helmfile -C /usr/local/bin/ && \
    rm -f helmfile_${HELMFILE_VER}_linux_amd64.tar.gz

RUN apk --update add openjdk7-jre && \
    gcloud components install app-engine-java kubectl gke-gcloud-auth-plugin
