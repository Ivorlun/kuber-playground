# Kuber Playground

## Summary

This project exists for experiments with kubernetes infrastructure base layer for microservices-like applications.

Besides manifests and charts of the typical application itself it contains basic necessary parts like:
- Infrastructure code for the cluster creation with Static IP
- Ingress controller with automatic TLS manager
- Central metrics and logs aggregators with persistence enabled
- All described above is embedded into CI-CD pipeline

For now it is vendor locked - so check Tools in use section to determine is it relevant for you.
So using this repo allows you to create automatically GKE cluster with Terraform and Gitlab CI to deploy your application into kubernetes or test your hypothesis about it.

**Please note that development is still in progress so there are a lot of improvements to be done for secure and stable use**

## Tools in use

- Gitlab (as you may noticed) as main development platform and CI-CD instrument
- Google Kubernetes Engine (Standard) as k8s cluster provider
- Terraform for IaC
- Helm + Helmfile as kubernetes service and main applications package managers
- Ingress-nginx + Cert-Manager for the secure connections outside
- Kube-Prometheus-Stack (Prometheus + Grafana + Alertmanager) as monitoring
- Loki+Promtail as an alternative for the EFK/ELK log aggregator stack

## Typical microservices application - Online Boutique

Google Online Boutique (microservices demo aka hipster shop) consists of 11 parts which communicate via grpc.

Main value of its app for this project is that it contains all common types of microservices and kubernetes objects which can be packed and deployed in a variety of ways.

Detailed documentation located here - [Online Boutique Docs](docs/README.md).

Source code repo - https://github.com/GoogleCloudPlatform/microservices-demo.

## Configuration
### Requirements
To use this repo you should have a Google Cloud Account and Gitlab.

### TL;DR

All required configuration makes via Gitlab variables.
Necessary:
1. BASE64_GOOGLE_CREDENTIALS - Google Service account credentials for managing computer resources
1. CLOUDSDK_COMPUTE_ZONE - variables for omitting configuration default values
1. CLOUDSDK_CORE_PROJECT - variables for omitting configuration default values
1. TELEGRAM_BOT_TOKEN - for receiving alerts from the alertmanager
1. TELEGRAM_CHAT_ID - for receiving alerts from the alertmanager
1. TF_VAR_gcp_project - name of the GCP project which is used for the cluster creation

Optional:
1. TF_VAR_agent_token - for terraform in case of Gitlab kubernetes agent use
1. TF_VAR_kas_address - for terraform in case of Gitlab kubernetes agent use
1. DOCKER_CONFIG_BASE64 - in case if you prefer other than gitlab private repo

And don't forget to change ingress addresses as well as cert-manager email in issuers to your own in values yaml files and manifests.

### Create your GCP credentials
To set up your project to communicate to GCP and the GitLab API:

1. To authenticate GCP with GitLab, create a GCP service account with following roles: `Compute Network Viewer`, `Kubernetes Engine Admin`, `Service Account User`, and `Service Account Admin`. Both User and Admin service accounts are necessary. The User role impersonates the default service account when creating the node pool. The Admin role creates a service account in the kube-system namespace.
1. Download the JSON file with the service account key you created in the previous step.
1. On your computer, encode the JSON file to base64 (replace /path/to/sa-key.json to the path to your key):
    * `base64 /path/to/sa-key.json | tr -d \\n`

Use the output of this command as the `BASE64_GOOGLE_CREDENTIALS` environment variable in the next step.


### Required CI configuration:

1. Execute base64 `/path/to/sa-key.json | tr -d \\n`
1. In project, select Settings > CI/CD.
1. Expand Variables.
1. Set the variable `BASE64_GOOGLE_CREDENTIALS` to the base64 encoded JSON file you just created.
1. Set the variable `TF_VAR_gcp_project` to your GCP project name.

### Terraform configuration:

This values could be set via variables, command line arguments or via CI/CD variables.

The original file variables.tf contains other variables that you can override according to your needs:

* `TF_VAR_gcp_region`: Set your cluster’s region.
* `TF_VAR_cluster_name`: Set your cluster’s name.
* `TF_VAR_cluster_description`: Set a description for the cluster. We recommend setting this to `$CI_PROJECT_URL` to create a reference to your GitLab project on your GCP cluster detail page. This way you know which project was responsible for provisioning the cluster you see on the GCP dashboard.
* `TF_VAR_machine_type`: Set the machine type for the Kubernetes nodes.
* `TF_VAR_initial_node_count`: Set the number of Kubernetes nodes.

There is an option to install base infra layer (monitoring, logging and ingress with tls) via terraform helm provider like it is done for gitlab agent.

If you would like to connect cluster with Gitlab via Gitlab Kubernetes agent:
* Set the variable `TF_VAR_agent_token` to the agent token displayed in the previous task if Gitlab agent is enabled.
* Set the variable `TF_VAR_kas_address` to the agent server address displayed in the previous task Gitlab agent is enabled.
* Set the variable `TF_VAR_agent_namespace`: Set the Kubernetes namespace for the GitLab agent.

### Domains and static address

Configurations like DNS domains and records are out of scope right now.

### Ingress and Cert-manager

Do not forget to change cert-manager [issuers](./infra/ingress/cluster-issuers/cluster-issuers.yaml) email and application ingress domain in all values.yaml.
Ingress controller should point to domain and address under your control otherwise Let's Encrypt will not approve certificate requests.


### Alerting

#### Telegram bot

For now solution assumes that telegram is your main receiver of the alerts.
To configure it you need to create secret variable with base64 encoded TELEGRAM_BOT_TOKEN and TELEGRAM_CHAT_ID variables

1. Go to https://t.me/botfather on Telegram
1. Create a bot and give it groups privileges
1. Copy its token - it is required for the alerting service as token
1. Create a group which will be used to receive alert messages
1. Add to it your bot and `@RawDataBot`
1. After its addition copy chat id, starting with `-`
1. CI automatically creates opaque secret for shim service
```bash
kubectl create secret generic telegram-creds -n observability
        --from-literal=chatID=${chatID}
        --from-literal=botToken=${botToken}
```


## Connect to the cluster after installation

1. Go to https://cloud.google.com/sdk/docs/install, obtain `gcloud` utility and add it to PATH
1. Perform `gcloud init` setting your gcloud project with cluster as default
1. Run `gcloud components install kubectl gke-gcloud-auth-plugin`
1. Then execute `gcloud container clusters get-credentials --region=europe-west4-a  gitlab-terraform-gke`

## Secret management

To be lightweight and kube-native project uses Sealed-secrets from Bitnami to manage secrets.

For detailed documentation please check the official page.

Here is short intro into it.

Sealed Secrets is composed of two parts:

- A cluster-side controller / operator
- A client-side utility: `kubeseal`

The `kubeseal` utility uses asymmetric crypto to encrypt secrets that only the controller can decrypt.

These encrypted secrets are encoded in a `SealedSecret` resource, which you can see as a recipe for creating
a secret. Here is how it looks:

```yaml
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: mysecret
  namespace: mynamespace
spec:
  encryptedData:
    foo: AgBy3i4OJSWK+PiTySYZZA9rO43cGDEq.....
```

Once unsealed this will produce a secret equivalent to this:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
  namespace: mynamespace
data:
  foo: YmFy  # <- base64 encoded "bar"
```


#### Usage
Note the SealedSecret and Secret must have the same namespace and name. This is a feature to prevent other users on the same cluster from re-using your sealed secrets.
```bash
# Create a json/yaml-encoded Secret somehow:
# (note use of `--dry-run` - this is just a local file!)
echo -n bar | kubectl create secret generic mysecret --dry-run=client --from-file=foo=/dev/stdin -o json >mysecret.json

# This is the important bit:
# (note default format is json!)
kubeseal <mysecret.json >mysealedsecret.json

# At this point mysealedsecret.json is safe to upload to Github,
# post on Twitter, etc.

# Eventually:
kubectl create -f mysealedsecret.json

# Profit!
kubectl get secret mysecret
```
#### Managing existing secrets

If you want SealedSecret controller to take management of an existing Secret (i.e. overwrite it when unsealing a SealedSecret with the same name and namespace), then you have to annotate that Secret with the annotation `sealedsecrets.bitnami.com/managed: "true"` ahead applying the Usage steps.

#### Update existing secrets

If you want to add or update existing sealed secrets without having the cleartext for the other items, you can just copy&paste the new encrypted data items and merge it into an existing sealed secret.

You must take care of sealing the updated items with a compatible name and namespace (see note about scopes above).

You can use the `--merge-into` command to update an existing sealed secrets if you don't want to copy&paste

### Notes and fine tuning

If you find some manifests or code excess please make a check before its removal because there are some unclear challenges which i faced during configuration. I try to describe some of them below.


### Runner init
In case if you are exceeding Gitlab CI free limits you should it is very straight forward and can be done via few commands below:

```bash
apt update && DEBIAN_FRONTEND=noninteractive apt upgrade -y && autoremove -y
curl -sSL https://get.docker.com | bash
curl -sSL "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt install -y gitlab-runner
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token <TOKEN_HERE> \
  --executor docker \
  --description "Some-runner" \
  --docker-image "docker:20.10.16" \
  --docker-privileged \
  --docker-volumes "/certs/client"
```


#### Nginx cluster role

This is already done in CI, but you you would like to change it remember that to use ingress-nginx ingress in GKE cluster role binding for the creating account should be set.

Details - https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke
```bash
    - kubectl create clusterrolebinding cluster-admin-binding
        --clusterrole cluster-admin
        --user $(gcloud config get-value account)
```


### Add grafana dashboards and datasources

There are a few methods to import dashboards to Grafana:
1. Add dashboards to the source code dir inside chart with kind:dashboard
1. Enable sidecar that will watch for configmaps with label **"dashboard=1"** and json within. This way is used.

But you should be careful due to datasources should have static uid to prevent configuration drift during dashboards import.
So in this repo datasources named `prometheus` and `loki` respectively without generated uid.

Also old versions of the loki-stack chart does not support datasource setting, so you should do it via grafana chart configuration:
```yaml
  additionalDataSources:
  - name: loki
    isDefault: false
    type: loki
    url: http://loki:3100
    uid: loki
```

#### Grafana dashboards examples
For correct work uid and default to false should be set:
```yaml
loki:
  isDefault: false
  datasource:
    uid: "loki"
```

Then this kind of cm should be deployed with corresponding label.
This is header of the file, dashboard should
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  labels:
    grafana_dashboard: "1"
  name: name-of-the-dashboard
  namespace: observability
data:
  name-of-the-dashboard.json: |-
    <JSON_DASHBOARD_HERE>
```


Check this guides and issues below for more info:
* https://artifacthub.io/packages/helm/grafana/grafana/6.50.7#sidecar-for-dashboards
* https://github.com/prometheus-community/helm-charts/issues/1370
* https://github.com/grafana/grafana/issues/10786

### Testing apps

There are some apps I would recommend as debugging for the ingress and TLS:

1. Kuard - tiny and basic app which shows you info about pod and its environment
2. PodInfo by Stefan Prodan - app with common for webapps API methods which could be used to test your infra


### Terraform ignore init node count

There is an issue with autoscaling deployment that tf apply will always destroy or recreate your current nodes if it does not equals to initial values.

To prevent it this block was added.
```json
  lifecycle {
    ignore_changes = [
      initial_node_count
    ]
  }
```
Details:

https://github.com/hashicorp/terraform-provider-google/issues/6889

https://github.com/hashicorp/terraform-provider-google/issues/6901#issuecomment-667369691


## Prometheus persistence

In this repo standard storage classes are used but if you would like to switch to s3 or gcs buckets check this documentation please:

https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/user-guides/storage.md#storage-provisioning-on-aws


## Loki persistence

Loki-stack contains very old Loki version in it - there is only single type of the deployment with old way of the persistent volumes and rules.

New one has HA version so values is completely different.

https://grafana.com/docs/loki/latest/storage/#gcp-deployment-gcs-single-store

This one is taken from the loki helm chart source code:
```yaml
  {{- if and (semverCompare ">= 1.23-0" .Capabilities.KubeVersion.Version) (.Values.singleBinary.persistence.enableStatefulSetAutoDeletePVC)  }}
  {{/*
    Data on the singleBinary nodes is easy to replace, so we want to always delete PVCs to make
    operation easier, and will rely on re-fetching data when needed.
  */}}
```

## GKE Free tiers

There is great guide about managed kubernetes options on major cloud providers - https://github.com/Neutrollized/free-tier-gke.

Long story short - you can use preemptible medium-2 machines with GCP paying under 10 dollars monthly.

Also each provider has trial period with lot of resources available for some time.