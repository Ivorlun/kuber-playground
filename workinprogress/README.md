# Some ideas and snippets for future development

### GitOps Flux Monitoring installation

Flux suggests to install itself with kube-prometheus-stack and loki-promtail out of the box:
https://fluxcd.io/flux/guides/monitoring/

Manifests are located here:
https://github.com/fluxcd/flux2/tree/main/manifests/monitoring

### Mixin for auto deploy rules and dashboards
Cert-manager prometheus rules and dashboards
https://gitlab.com/uneeq-oss/cert-manager-mixin


problem - which to install first: prometheus or ingress


#### Dashboards list

https://grafana.com/grafana/dashboards/14055-loki-stack-monitoring-promtail-loki/

https://grafana.com/grafana/dashboards/13332-kube-state-metrics-v2/



### kube-prom helm
```bash
helm upgrade --install --namespace observability
      --create-namespace --set namespaceOverride=observability
      --repo https://prometheus-community.github.io/helm-charts
      -f monitoring/prom-operator.values.yaml
      prom-operator kube-prometheus-stack
```